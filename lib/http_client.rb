require "http_client/version"

module Client
  require 'net/http'
  require 'uri'
  require 'json'
  attr_accessor :url_base, :protocol
end

class Http
  include Client

  def initialize(url_base)
    @url_base = url_base
  end

  def get(method,params=nil)
    url = URI(@url_base+"/"+method)
    Net::HTTP.start(url.host, url.port) do |http|
      request = Net::HTTP::Get.new(url)
      if params
        params.each do |key, value|
          request[key] = value
        end
      end
      return http.request(request)
    end
  end

  def post(method,params=nil,body=nil)
    url = URI(@url_base+"/"+method)
    Net::HTTP.start(url.host, url.port) do |http|
      request = Net::HTTP::Post.new(url)
      if params
        params.each do |key, value|
          request[key] = value
        end
      end
      request.body = body.to_json if body
      return http.request(request)
    end
  end

  def put(method,params=nil,body=nil)
    url = URI(@url_base+"/"+method)
    Net::HTTP.start(url.host, url.port) do |http|
      request = Net::HTTP::Put.new(url)
      if params
        params.each do |key, value|
          request[key] = value
        end
      end
      request.body = body.to_json if body
      return http.request(request)
    end
  end

  def delete(method,params=nil,body=nil)
    url = URI(@url_base+"/"+method)
    Net::HTTP.start(url.host, url.port) do |http|
      request = Net::HTTP::Delete.new(url)
      if params
        params.each do |key, value|
          request[key] = value
        end
      end
      request.body = body.to_json if body
      return http.request(request)
    end
  end

end

class Https
  include Client

  def initialize(url_base)
    @url_base = url_base
  end

  def get(method,params=nil)
    url = URI(@url_base+"/"+method)
    Net::HTTP.start(url.host, url.port, :use_ssl => url.scheme == 'https') do |http|
      request = Net::HTTP::Get.new(url)
      if params
        params.each do |key, value|
          request[key] = value
        end
      end
      return http.request(request)
    end
  end

  def post(method,params=nil,body=nil)
    url = URI(@url_base+"/"+method)
    Net::HTTP.start(url.host, url.port, :use_ssl => url.scheme == 'https') do |http|
      request = Net::HTTP::Post.new(url)
      if params
        params.each do |key, value|
          request[key] = value
        end
      end
      request.body = body.to_json if body
      return http.request(request)
    end
  end

  def put(method,params=nil,body=nil)
    url = URI(@url_base+"/"+method)
    Net::HTTP.start(url.host, url.port, :use_ssl => url.scheme == 'https') do |http|
      request = Net::HTTP::Put.new(url)
      if params
        params.each do |key, value|
          request[key] = value
        end
      end
      request.body = body.to_json if body
      return http.request(request)
    end
  end

  def delete(method,params=nil,body=nil)
    url = URI(@url_base+"/"+method)
    Net::HTTP.start(url.host, url.port, :use_ssl => url.scheme == 'https') do |http|
      request = Net::HTTP::Delete.new(url)
      if params
        params.each do |key, value|
          request[key] = value
        end
      end
      request.body = body.to_json if body
      return http.request(request)
    end
  end

end
