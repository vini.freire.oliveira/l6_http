# L6_Http

* Gem to make request using http protocol

version: 0.1.0;

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'l6_http'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install l6_http

## Usage 
Gem to make http and https request

* require "http_client"

##http:
* Http.new("url_base").get("method")
* Http.new("url_base").get("method",header)
* how to use:
    - header = {"Content-Type":"application/json","cache-control":"no-cache"}
    - res = Http.new("url_base").get("method",header)
* below shows how get body and header of http response 

##https:
* Https.new(url_base).get("method")
* Https.new(url_base).get("method",header)
* Https.new(url_base).post("method",header,body)
* how to use:
    - header = {"Content-Type":"application/json","cache-control":"no-cache"}
    - body = {"email":"nome@email.com","password":"pass121"}
    - res = Https.new("url_base").get("method",header,body)

## http response
- Net::HTTP
* documentation
- https://docs.ruby-lang.org/en/2.0.0/Net/HTTP.html

# How get Headers
- res['Set-Cookie']            # => String
- res.get_fields('set-cookie') # => Array
- res.to_hash['set-cookie']    # => Array
- puts "Headers: #{res.to_hash.inspect}"

# How get Status
- puts res.code       # => '200'
- puts res.message    # => 'OK'
- puts res.class.name # => 'HTTPOK'

# How get Body
- puts res.body if res.response_body_permitted?

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/vini.freire.oliveira/l6_http. This project is intended to be a safe,welcoming space for collaboration, and contributors are expected to adhere to the code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the HttpClient project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/http_client/blob/master/CODE_OF_CONDUCT.md).
