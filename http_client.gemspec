
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "http_client/version"

Gem::Specification.new do |spec|
  spec.name          = "l6_http"
  spec.version       = HttpClient::VERSION
  spec.authors       = ["Vinicius Freire"]
  spec.email         = ["vini.freire.oliveira@gmail.com"]

  spec.summary       = %q{gem to make http and https request}
  spec.description   = %q{gem to make http and https request}
  spec.homepage      = "https://gitlab.com/vini.freire.oliveira/l6_http"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
